from django.contrib import admin

from tools_for_dissent.models import Tool, DownloadLinks, Type, Recommendation, Petition, Fund, \
    BlogPage, BlogIndexPage


@admin.register(Type)
class TypeAdmin(admin.ModelAdmin):
    list_display = ['name']
    exclude = ['tools', ]


@admin.register(DownloadLinks)
class DownloadLinksAdmin(admin.ModelAdmin):
    exclude = ['tools', ]


@admin.register(Recommendation)
class RecommendationAdmin(admin.ModelAdmin):
    list_display = ['name', 'website']


@admin.register(BlogPage)
class BlogPageAdmin(admin.ModelAdmin):
    list_display = ['title', 'date']


class TypeInline(admin.TabularInline):
    model = Type.tools.through
    verbose_name = 'Type'
    verbose_plural_name = 'Types'


class DownloadLinksInline(admin.TabularInline):
    model = DownloadLinks
    verbose_name = 'Download Link'


@admin.register(Tool)
class ToolAdmin(admin.ModelAdmin):
    list_display = ['name', 'website']

    inlines = [
        DownloadLinksInline,
        TypeInline
    ]


@admin.register(Fund)
class FundAdmin(admin.ModelAdmin):
    list_display = ['name', 'cause', 'link']


@admin.register(Petition)
class PetitionAdmin(admin.ModelAdmin):
    list_display = ['name', 'link']
