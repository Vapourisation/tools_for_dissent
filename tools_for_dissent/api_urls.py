from django.urls import path
from tools_for_dissent import api

urlpatterns = [
    path('v1/tools/', api.get_tools, name='all_tools'),
    path('v1/encrypt_text/', api.encrypt_text, name='encrypt_text'),
    path('v1/decrypt_text/', api.decrypt_text, name='decrypt_text'),
    path('v1/delete_file', api.delete_file, name='delete_file')
]
