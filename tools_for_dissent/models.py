import string
import random

from django.core.validators import MaxValueValidator, MinValueValidator, URLValidator
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.text import slugify

from taggit.managers import TaggableManager
from tinymce.models import HTMLField

from modelcluster.fields import ParentalKey

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.name)
    object = instance.__class__
    qs_exists = object.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
            slug=slug, randstr=random_string_generator(size=4))

        return unique_slug_generator(instance, new_slug=new_slug)
    return slug


class Tool(models.Model):
    name = models.CharField(max_length=4096)
    website = models.URLField()
    image = models.ImageField()
    description = models.TextField()
    details = HTMLField(null=True, blank=True)
    main_type = models.ForeignKey('Type', on_delete=models.CASCADE, null=True, blank=True)
    slug = models.SlugField(max_length=250, null=True, blank=True)
    security_score = models.FloatField(default=0.0, validators=[
        MinValueValidator(0.0), MaxValueValidator(10.0)
    ], help_text='Based on how secure the tool/software is deemed to be.')
    anonymity_score = models.FloatField(default=0.0, validators=[
        MinValueValidator(0.0), MaxValueValidator(10.0)
    ], help_text='Based on how anonymously one could use this tool.')
    tags = TaggableManager()

    @property
    def get_absolute_url(self):
        path = reverse('tool_page', args=[self.slug])
        return path

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.slug = unique_slug_generator(self)
        return super(Tool, self).save()

    def __str__(self):
        return self.name


class Type(models.Model):
    name = models.CharField(max_length=1024)
    tools = models.ManyToManyField(Tool, related_name='types', blank=True)

    def __str__(self):
        return self.name


class DownloadLinks(models.Model):
    device = models.CharField(max_length=1024)
    link = models.URLField(validators=[URLValidator])
    tools = models.ForeignKey(Tool, related_name='download_links', on_delete=models.CASCADE)
    icon_name = models.CharField(max_length=512, null=True, blank=True)

    def __str__(self):
        return f'{self.tools.name} - {self.device} link'

    class Meta:
        verbose_name_plural = 'Download Links'


class Recommendation(models.Model):
    name = models.CharField(max_length=512, help_text='The tools name, not yours. Maximum of 512 characters')
    website = models.URLField(
        help_text='Should be a URL but doesn\'t need to be to the exact site. Can be an article.',
        validators=[URLValidator]
    )
    description = models.TextField(help_text='No real limit but try and keep it short. Someone has to read this.')
    reason = models.TextField(help_text='Be passionate about why. This matters.')

    def __str__(self):
        return f'{self.name} - {self.website}'


class ProtestingTipArticle(models.Model):
    link = models.URLField()


class ProtestingTip(models.Model):
    tip = models.TextField()
    icon = models.ImageField()
    link_to_related_article = models.ManyToManyField(ProtestingTipArticle)
    tags = TaggableManager()

    class Meta:
        verbose_name_plural = 'Protesting Tips'


class Petition(models.Model):
    name = models.CharField(max_length=512)
    link = models.URLField()
    description = HTMLField()
    slug = models.SlugField(max_length=250, null=True, blank=True)
    tags = TaggableManager()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.slug = unique_slug_generator(self)
        return super(Petition, self).save()


class Fund(models.Model):
    name = models.CharField(max_length=512)
    cause = models.CharField(max_length=1024)
    link = models.URLField()
    description = HTMLField()
    slug = models.SlugField(max_length=250, null=True, blank=True)
    tags = TaggableManager()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.slug = unique_slug_generator(self)
        return super(Fund, self).save()


# Wagtail Blog Models
class BlogPage(Page):
    body = RichTextField()
    date = models.DateField("Post date")
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    tags = TaggableManager()

    def get_context(self, request, *args, **kwargs):
        context = super(BlogPage, self).get_context(request, *args, **kwargs)
        context['posts'] = self.related_links.all()
        context['children'] = self.get_children()
        context['descendents'] = self.get_descendants()
        context['blog_page'] = self

        context['menuitems'] = self.get_children().filter(
            live=True, show_in_menus=True)

        return context

    # Search index configuration
    search_fields = Page.search_fields + [
        index.SearchField('body'),
        index.FilterField('date'),
    ]

    # Editor panels configuration
    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('body', classname="full"),
        InlinePanel('related_links', label="Related links"),
    ]

    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
        ImageChooserPanel('feed_image'),
    ]

    # Parent page / subpage type rules
    # parent_page_types = ['tools_for_dissent.BlogIndex']
    # subpage_types = []

    def __str__(self):
        return self.title


class BlogPageRelatedLink(Orderable):
    page = ParentalKey(BlogPage, on_delete=models.CASCADE, related_name='related_links')
    name = models.CharField(max_length=255)
    url = models.URLField()

    panels = [
        FieldPanel('name'),
        FieldPanel('url'),
    ]


class BlogIndexPage(Page):
    def get_context(self, request):
        context = super().get_context(request)

        # Add extra variables and return the updated context
        context['blog_entries'] = BlogPage.objects.child_of(self).live()
        return context
